import {combineReducers} from 'redux'
import createStore from './createStore'
import shiftsReducer from './shiftsReducer'
import peopleReducer from './peopleReducer'

const catapultReducer = combineReducers({
  shifts: shiftsReducer,
  people: peopleReducer,
})

export default createStore(catapultReducer)
