const InitialState = {}

const peopleReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'FETCH_INVITED_CONTRACTS_LIST_FULFILLED': {
      return Object.assign({}, state, {[action.meta.roleId]: {
        fetching: false,
        list: action.payload.data,
      }})
    }

    case 'FETCH_INVITED_CONTRACTS_LIST_PENDING': {
      return Object.assign({}, state, {[action.meta.roleId]: {
        fetching: true,
        list: [],
      }})
    }

    default:
      return state
  }
}

export default peopleReducer
