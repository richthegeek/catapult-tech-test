import {connect} from 'react-redux'
import React from 'react'
import PropTypes from 'prop-types'
import {fetchShifts} from '../../actions/shiftsActions'
import ShiftsList from './ShiftsList'
import ShiftsFilter from './ShiftsFilter'

function ShiftsContainer({fetchShiftsList, fetchingShifts, shiftsList}) {
  const [filter, setFilter] = React.useState({
    type: '', // filter by e.g. 'Waiting staff'
    time: '', // filter by am/pm start time
  })

  const setType = (type) => setFilter({...filter, type})
  const setTime = (time) => setFilter({...filter, time})

  // fetch the data from the server on mount and
  // whenever the filter is changed.
  React.useEffect(() => {
    const query = {}
    if (filter.type !== '') {
      query.jobType = filter.type
    }
    if (filter.time === 'am') {
      query.startTime_lte = '11:59'
    }
    if (filter.time === 'pm') {
      query.startTime_gte = '12:00'
    }

    fetchShiftsList(query)
  }, [filter])

  return (
    <div>
      <ShiftsFilter type={filter.type} setType={setType} time={filter.time} setTime={setTime} />
      {
        fetchingShifts
          ? <h2>Loading</h2>
          : <ShiftsList list={shiftsList} />
      }
    </div>
  )
}

ShiftsContainer.propTypes = {
  shiftsList: PropTypes.arrayOf(PropTypes.shape({
    roleId: PropTypes.number.isRequired,
    shiftDate: PropTypes.string.isRequired,
    startTime: PropTypes.string.isRequired,
    endTime: PropTypes.string.isRequired,
    staff_required: PropTypes.number.isRequired,
    number_of_invited_staff: PropTypes.number.isRequired,
    jobType: PropTypes.string.isRequired,
  })).isRequired,
  fetchingShifts: PropTypes.bool.isRequired,
  fetchShiftsList: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  shiftsList: state.shifts.shiftsList,
  fetchingShifts: state.shifts.fetchingShifts,
})

const mapDispatchToProps = (dispatch) => ({
  fetchShiftsList: (filter) => dispatch(fetchShifts(filter)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ShiftsContainer)
