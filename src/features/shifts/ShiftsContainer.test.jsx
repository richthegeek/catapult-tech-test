import React from 'react'
import assert from 'assert'
import {shallow, mount} from 'enzyme'
import { act } from 'react-dom/test-utils'
import api from '../../../config/api'
import ShiftsContainerWrapped from './ShiftsContainer'

const shiftsList = api().shifts_list

const ShiftsContainer = ShiftsContainerWrapped.WrappedComponent

const getProps = (newProperties) => Object.assign({
  fetchShiftsList: jest.fn(),
  fetchingShifts: true,
  shiftsList: [],
}, newProperties)

describe('ShiftsContainer', () => {
  describe('on first load', () => {
    it('should load data (fetchShiftsList)', () => {
      const props = getProps()
      mount(<ShiftsContainer {...props} />)

      assert.equal(props.fetchShiftsList.mock.calls.length, 1)
    })

    it('should render a `loading` state and filters, but no list', () => {
      const props = getProps({fetchingShifts: true})
      const wrap = shallow(<ShiftsContainer {...props} />)

      assert.equal(wrap.find('h2').length, 1)
      assert.equal(wrap.find('ShiftsFilter').length, 1)
      assert.equal(wrap.find('ShiftsList').length, 0)

      assert.equal(wrap.find('ShiftsFilter').prop('type'), '')
      assert.equal(wrap.find('ShiftsFilter').prop('time'), '')
    })
  })

  it('should pass loaded data to the ShiftsList', () => {
    const props = getProps({fetchingShifts: false, shiftsList})
    const wrap = shallow(<ShiftsContainer {...props} />)

    assert.equal(wrap.find('h2').length, 0)
    assert.equal(wrap.find('ShiftsFilter').length, 1)
    assert.equal(wrap.find('ShiftsList').length, 1)

    assert.equal(wrap.find('ShiftsList').prop('list'), shiftsList)
  })

  it('should load new data when the `type` filter is changed', () => {
    const props = getProps()
    const wrap = mount(<ShiftsContainer {...props} />)

    props.fetchShiftsList.mockReset()

    act(() => wrap.find('ShiftsFilter').prop('setType')('Barista'))

    assert.equal(props.fetchShiftsList.mock.calls.length, 1)
    assert.equal(props.fetchShiftsList.mock.calls[0][0].jobType, 'Barista')
    assert.equal(props.fetchShiftsList.mock.calls[0][0].startTime_lte, undefined)
    assert.equal(props.fetchShiftsList.mock.calls[0][0].startTime_gte, undefined)
  })

  it('should load new data when the `time` filter is changed to am', () => {
    const props = getProps()
    const wrap = mount(<ShiftsContainer {...props} />)

    props.fetchShiftsList.mockReset()

    act(() => wrap.find('ShiftsFilter').prop('setTime')('am'))

    assert.equal(props.fetchShiftsList.mock.calls.length, 1)
    assert.equal(props.fetchShiftsList.mock.calls[0][0].jobType, undefined)
    assert.equal(props.fetchShiftsList.mock.calls[0][0].startTime_lte, '11:59')
    assert.equal(props.fetchShiftsList.mock.calls[0][0].startTime_gte, undefined)
  })

  it('should load new data when the `time` filter is changed to pm', () => {
    const props = getProps()
    const wrap = mount(<ShiftsContainer {...props} />)

    props.fetchShiftsList.mockReset()

    act(() => wrap.find('ShiftsFilter').prop('setTime')('pm'))

    assert.equal(props.fetchShiftsList.mock.calls.length, 1)
    assert.equal(props.fetchShiftsList.mock.calls[0][0].jobType, undefined)
    assert.equal(props.fetchShiftsList.mock.calls[0][0].startTime_lte, undefined)
    assert.equal(props.fetchShiftsList.mock.calls[0][0].startTime_gte, '12:00')
  })
})
