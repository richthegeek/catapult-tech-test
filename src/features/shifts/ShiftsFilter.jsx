import React from 'react'
import PropTypes from 'prop-types'

const types = [
  'Barista',
  'Receptionist',
  'Retail store shift',
  'Security',
  'Waiting staff',
]
const times = ['am', 'pm']

export default function ShiftsFilter(props) {
  const onTypeChange = (e) => props.setType(e.target.value)
  const onTimeChange = (e) => props.setTime(e.target.value)

  return (
    <div>
      <select name='type' value={props.type} onChange={onTypeChange}>
        <option value=''>Any type</option>
        <optgroup label='Filter by type'>
          {types.map((type) => (
            <option key={type} value={type}>{type}</option>
          ))}
        </optgroup>
      </select>

      <select name='time' value={props.time} onChange={onTimeChange}>
        <option value=''>Any time</option>
        <optgroup label='Filter by shift start time'>
          {times.map((time) => (
            <option key={time} value={time}>{time}</option>
          ))}
        </optgroup>
      </select>
    </div>
  )
}

ShiftsFilter.propTypes = {
  type: PropTypes.oneOf(['', ...types]),
  setType: PropTypes.func.isRequired,
  time: PropTypes.oneOf(['', ...times]),
  setTime: PropTypes.func.isRequired,
}

ShiftsFilter.defaultProps = {
  type: '',
  time: '',
}
