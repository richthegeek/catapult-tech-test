import React from 'react'
import assert from 'assert'
import {shallow} from 'enzyme'
import InvitedContractsContainerWrapped from './invitedContractsContainer'

const InvitedContractsContainer = InvitedContractsContainerWrapped.WrappedComponent

const getProps = (newProperties) => Object.assign({
  roleId: 12,
  requested: false,
  fetching: false,
  list: [],
  fetchPeople: jest.fn(),
}, newProperties)

describe('InvitedContractsContainer', () => {
  describe('when data is unrequested', () => {
    it('should render a button which is clickable', () => {
      const wrap = shallow(<InvitedContractsContainer {...getProps()} />)

      const button = wrap.find('button')
      assert.equal(button.length, 1)
      assert.ok(button.prop('onClick'))
    })

    it('should trigger the fetchPeople prop when clicked', () => {
      const props = getProps()
      const wrap = shallow(<InvitedContractsContainer {...props} />)

      const button = wrap.find('button')
      button.simulate('click')

      assert.equal(props.fetchPeople.mock.calls.length, 1)
      assert.equal(props.fetchPeople.mock.calls[0][0], 12)
    })
  })

  describe('when data is requested, but not fetched', () => {
    it('should render a disabled loading button', () => {
      const props = getProps({requested: true, fetching: true})
      const wrap = shallow(<InvitedContractsContainer {...props} />)

      const button = wrap.find('button')
      assert.equal(button.length, 1)
      assert.equal(button.text(), 'Loading...')
      assert.ok(button.prop('disabled'))
      assert.ok(!button.prop('onClick'))
    })
  })

  describe('when data is loaded', () => {
    it('should render the list of people after loading', () => {
      const props = getProps({
        requested: true,
        fetching: false,
        list: [{id: 1, candidateName: 'Bobby'}, {id: 14, candidateName: 'Timmy'}],
      })
      const wrap = shallow(<InvitedContractsContainer {...props} />)

      assert.equal(wrap.find('button').length, 0)

      const items = wrap.find('li')
      assert.equal(items.length, 2)

      assert.equal(items.at(0).key(), 1)
      assert.equal(items.at(0).text(), 'Bobby')

      assert.equal(items.at(1).key(), 14)
      assert.equal(items.at(1).text(), 'Timmy')
    })

    it('should render an oops text if the list was empty after loading', () => {
      const props = getProps({
        requested: true,
        fetching: false,
        list: [],
      })
      const wrap = shallow(<InvitedContractsContainer {...props} />)

      assert.equal(wrap.find('button').length, 0)

      assert.equal(wrap.find('li').length, 1)
      assert.equal(wrap.find('li i').length, 1)
    })
  })
})
