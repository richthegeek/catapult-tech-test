import {connect} from 'react-redux'
import React from 'react'
import PropTypes from 'prop-types'

import {fetchPeople} from '../../actions/peopleActions'

/*
  InvitedContractsContainer has three general states:
    - no data has been requested (requested = false)
    - data is being fetched (fetching = true)
    - data is loaded (requested = true, fetching = false)

  These states are managed by the peopleReducer, perhaps too tightly coupled
*/
function InvitedContractsContainer(props) {
  const loadData = () => props.fetchPeople(props.roleId)

  if (!props.requested) {
    return <button onClick={loadData}>Invited Candidates</button>
  }

  if (props.fetching) {
    return <button disabled>Loading...</button>
  }

  return (
    <ul>
      {props.list.map((person) => (
        <li key={person.id}>{person.candidateName}</li>
      ))}
      {props.list.length === 0 && (
        <li><i>No invited candidates</i></li>
      )}
    </ul>
  )
}

InvitedContractsContainer.propTypes = {
  roleId: PropTypes.number.isRequired,
  fetchPeople: PropTypes.func.isRequired,
  requested: PropTypes.bool,
  fetching: PropTypes.bool,
  list: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    candidateName: PropTypes.string,
  })),
}

InvitedContractsContainer.defaultProps = {
  requested: false,
  fetching: false,
  list: [],
}

const mapStateToProps = (state, props) => ({
  ...(state.people[props.roleId] || {}),
  requested: props.roleId in state.people,
})

const mapDispatchToProps = (dispatch) => ({
  fetchPeople: (roleId) => dispatch(fetchPeople(roleId)),
})

export default connect(mapStateToProps, mapDispatchToProps)(InvitedContractsContainer)
